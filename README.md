# Fleet manager

## Developer setup

### Initial setup

You will need `docker` and `docker-compose` installed on your computer.

Follow the steps to have a running development environment.

```sh
$ poetry install
$ yarn install
$ cd ../docker/
$ ./gen_secrets.sh
$ DOCKER_BUILDKIT=1 COMPOSE_DOCKER_CLI_BUILD=1 docker-compose -p fman up --build
```

You probably want to use the CRI's [public OIDC test
client](https://cri.pages.epita.fr/services/intranet/oidc.html) when asked for
OIDC credentials.

Now, let's get you admin rights. Log in to http://localhost:8000.

```sh
# Open a new terminal, leaving docker-compose running in this one to get logs
$ cd docker/
$ ./manage.sh shell
```

```python
from django.contrib.auth import get_user_model
you = get_user_model().objects.first()
you.is_staff = True
you.is_superuser = True
you.save()
```

### Regular usage

```sh
$ cd docker/
$ DOCKER_BUILDKIT=1 COMPOSE_DOCKER_CLI_BUILD=1 docker-compose -p fman up --build
```

### Formatting

We use `python/black`, `eslint` and `prettier` as code formatters. They can be
enabled through git pre-commit with the following command:

```sh
$ poetry run pre-commit install
```

### Linting

We also use `pylint` (via `prospector`) to check the coding style. It is
configured with GitLab CI and produces warnings in GitLab. To check if the
project matches `pylint`'s recommendations you can use the following command:

```sh
$ poetry run prospector --profile base
```

### Services

#### Adminer
    URL: http://localhost:8010/
    System: 'PostgreSQL'
    User: 'fleet_manager_dev'
    Password in: docker/secrets/postgres-passwd
    Database: 'fleet_manager_dev'

    Have a look in `docker/docker-compose.yml` for other databases and
    credentials location.

#### Website
    URL: http://localhost:8000/
