from django.urls import include, path
from rest_framework.routers import DefaultRouter

from .views import SessionStateViewSet, SessionViewSet

router = DefaultRouter()
router.root_view_name = "sessions_state-api-root"
router.register("session-states", SessionStateViewSet)
router.register("sessions", SessionViewSet)

urlpatterns = [
    path("", include(router.urls)),
]
