import django_filters
from django.utils import timezone
from psycopg2.extras import DateTimeTZRange

from fleet.models import Room

from .models import Session, SessionState


class SessionBaseFilter(django_filters.FilterSet):
    room = django_filters.ModelChoiceFilter(
        queryset=Room.objects.all(), method="get_by_room"
    )

    latest = django_filters.BooleanFilter(method="get_latest")

    # pylint:disable=unused-argument
    def get_by_room(self, queryset, field_name, value):
        return queryset.by_room(value)

    # pylint:disable=unused-argument
    def get_latest(self, queryset, field_name, value):
        if not value:
            return queryset
        return queryset.latest_by_login()

    class Meta:
        fields = ("ip", "room", "latest")


class SessionStateAnonymousFilter(SessionBaseFilter):
    current = django_filters.BooleanFilter(method="get_current")

    # pylint:disable=unused-argument
    def get_current(self, queryset, field_name, value):
        if not value:
            return queryset
        return queryset.current()

    class Meta(SessionBaseFilter.Meta):
        model = SessionState
        fields = SessionBaseFilter.Meta.fields + ("current",)


class SessionStateFilter(SessionStateAnonymousFilter):
    class Meta(SessionStateAnonymousFilter.Meta):
        fields = SessionStateAnonymousFilter.Meta.fields + ("login",)


class SessionAnonymousFilter(SessionBaseFilter):
    since = django_filters.IsoDateTimeFilter(method="get_since")

    # pylint:disable=unused-argument
    def get_since(self, queryset, field_name, value):
        return queryset.filter(interval__overlap=DateTimeTZRange(value, timezone.now()))

    class Meta(SessionBaseFilter.Meta):
        model = Session
        fields = SessionBaseFilter.Meta.fields + ("since",)


class SessionFilter(SessionAnonymousFilter):
    class Meta(SessionAnonymousFilter.Meta):
        fields = SessionAnonymousFilter.Meta.fields + ("login",)
