from datetime import timedelta

from django.conf import settings
from django.core.management.base import BaseCommand, CommandError
from django.utils import timezone

from sessions_state import models


class Command(BaseCommand):
    help = "Delete session entries that are older than the age specified in settings"

    def handle(self, *args, **options):
        max_age = settings.SESSION_LOG_MAX_AGE
        self.stdout.write(f"Deleting sessions entries older than {max_age} days...")
        oldest_date = timezone.now() - timedelta(days=max_age)
        query = models.Session.objects.filter(interval__endswith__lt=oldest_date)
        res = None
        try:
            res = query.delete()
        except Exception as e:
            raise CommandError(f"ERROR: {e}") from e
        if res:
            self.stdout.write(self.style.SUCCESS(f"Deleted {res[0]} session entries"))
