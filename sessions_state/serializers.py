from drf_extra_fields.fields import DateTimeRangeField
from rest_framework import serializers

from .models import Session, SessionState


class SessionBaseSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        fields = (
            "url",
            "id",
            "ip",
            "image",
        )


class SessionStateAnonymousSerializer(SessionBaseSerializer):
    class Meta(SessionBaseSerializer.Meta):
        model = SessionState
        fields = SessionBaseSerializer.Meta.fields + ("timestamp",)


class SessionStateSerializer(SessionBaseSerializer):
    class Meta(SessionBaseSerializer.Meta):
        model = SessionState
        fields = SessionBaseSerializer.Meta.fields + (
            "login",
            "timestamp",
            "locked",
        )


class SessionSerializer(serializers.HyperlinkedModelSerializer):
    interval = DateTimeRangeField()

    class Meta(SessionBaseSerializer.Meta):
        model = Session
        fields = SessionBaseSerializer.Meta.fields + (
            "login",
            "interval",
        )


class SesssionStatsSerializer(serializers.Serializer):
    ip = serializers.CharField()
    duration = serializers.IntegerField()
