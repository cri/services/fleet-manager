from rest_framework.permissions import SAFE_METHODS, DjangoModelPermissions


class DjangoModelWithViewPermissions(DjangoModelPermissions):
    perms_map = {
        **DjangoModelPermissions.perms_map,
        "GET": ["%(app_label)s.view_%(model_name)s"],
    }


class SessionStatePermissions(DjangoModelWithViewPermissions):
    def has_permission(self, request, view):
        if request.method in SAFE_METHODS:
            if (
                "locked" not in request.query_params
                and "login" not in request.query_params
            ):
                return True
        return super().has_permission(request, view)
