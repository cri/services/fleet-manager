from ipaddress import ip_network

from django.conf import settings
from django.core.exceptions import ValidationError
from django.db import models
from django.db.models.expressions import RawSQL
from django.urls import reverse
from django.utils.timezone import now

from fleet import models as fleet_models


class Image(models.Model):
    title = models.CharField(
        max_length=256,
        unique=True,
    )

    name = models.CharField(
        max_length=256,
        blank=True,
    )

    kernel = models.URLField()

    initrd = models.URLField(
        blank=True,
    )

    cmdline = models.CharField(
        max_length=256,
        blank=True,
    )

    class Meta:
        ordering = ("name",)

    def _format(self, value):
        return value.format(
            title=self.title,
            name=self.name,
        )

    def get_kernel(self):
        return self._format(self.kernel)

    def get_kernel_filename(self):
        return self.get_kernel().rsplit("/", maxsplit=1)[-1]

    def get_initrd(self):
        return self._format(self.initrd)

    def get_initrd_filename(self):
        return self.get_initrd().rsplit("/", maxsplit=1)[-1]

    def get_cmdline(self, extra=""):
        return self._format(
            " ".join(
                filter(None, [self.cmdline, extra, settings.CMDLINE_OVERRIDE or ""])
            )
        )

    def __str__(self):
        return self.title


class ItemCategory(models.Model):
    title = models.CharField(
        max_length=256,
        unique=True,
    )

    label = models.CharField(
        max_length=80,
        blank=True,
    )

    priority = models.SmallIntegerField(
        default=0,
        blank=True,
    )

    class Meta:
        ordering = ("priority", "label")
        verbose_name_plural = "item categories"
        index_together = (("priority", "label"),)

    @staticmethod
    def categorize_items(items):
        items_with_category = [i for i in items if i.category is not None]
        items_without_category = [i for i in items if i.category is None]
        category = None
        separator = Item(
            label="",
            type="separator",
        )
        if not items_with_category:
            return list(items_without_category)
        result = []
        for item in items_with_category:
            if category != item.category:
                if category is not None:
                    result.append(separator)
                category = item.category
                if items_without_category and category.priority >= 0:
                    result.extend(list(items_without_category))
                    result.append(separator)
                    items_without_category = []
                if category and category.label:
                    result.append(category.to_separator())
            result.append(item)
        return result

    def to_separator(self):
        caret_count = (78 - len(self.label)) // 2
        label = "{carets} {label} {carets}".format(
            carets="-" * caret_count,
            label=self.label,
        )
        if len(label) < 80:
            label += "-"
        return Item(
            category=self,
            priority=-32768,
            label=label,
            type="separator",
        )

    def __str__(self):
        return self.title


class Item(models.Model):
    TYPE_CHOICES = (
        ("submenu", "Submenu"),
        ("img", "Image"),
        ("uri", "URI"),
        ("custom", "Custom script"),
        ("separator", "Separator"),
    )

    category = models.ForeignKey(
        ItemCategory,
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
    )

    priority = models.SmallIntegerField(
        default=0,
        blank=True,
    )

    label = models.CharField(max_length=80)

    type = models.CharField(
        max_length=10,
        choices=TYPE_CHOICES,
        db_index=True,
    )

    key = models.CharField(
        max_length=4,
        blank=True,
    )

    # type == 'submenu'

    submenu = models.ForeignKey(
        "Menu",
        blank=True,
        null=True,
        on_delete=models.CASCADE,
    )

    # type == 'img'

    image = models.ForeignKey(
        Image,
        blank=True,
        null=True,
        on_delete=models.CASCADE,
    )

    extra_cmdline = models.CharField(
        max_length=256,
        blank=True,
    )

    # type == 'uri'

    uri = models.URLField(
        blank=True,
    )

    # type == 'custom'

    script = models.TextField(
        blank=True,
    )

    class Meta:
        ordering = ("category__priority", "category__label", "priority", "label")

    def __str__(self):
        if not self.category:
            return f"{self.type} - {self.label}"
        label = f"[{self.category.label}]" if self.category.label else ""
        return f"{label}{self.type} - {self.label}"

    def get_cmdline(self):
        return self.image.get_cmdline(extra=self.extra_cmdline)

    def get_printable_key_ipxe(self):
        if not self.key:
            return " ${}" * 4

        if self.key[0:2] in ("0x", "0X"):
            value = int(self.key, 16)
        else:
            return self.key[0] + " ${}" * 2

        if value == 0x1B:
            return "ESC"

        if 0x01 <= value <= 0x1A:
            return f"C-{chr(ord('a') + value - 1)}"

        return " ${}" * 4

    def get_printable_key(self):
        if not self.key:
            return ""

        if self.key[0:2] in ("0x", "0X"):
            value = int(self.key, 16)
        else:
            return self.key[0]

        if value == 0x1B:
            return "ESC"

        if 0x01 <= value <= 0x1A:
            return f"C-{chr(ord('a') + value - 1)}"

        return ""

    def get_label(self):
        if self.type == "submenu":
            return f"{self.label}..."
        return self.label


class ItemGroup(models.Model):
    title = models.CharField(
        max_length=80,
        blank=True,
    )

    items = models.ManyToManyField(
        Item,
        blank=True,
    )

    categories = models.ManyToManyField(
        ItemCategory,
        blank=True,
    )

    def __str__(self):
        return self.title


class Menu(models.Model):
    title = models.CharField(max_length=80)

    item_groups = models.ManyToManyField(ItemGroup, blank=True)

    items = models.ManyToManyField(
        Item,
        blank=True,
    )

    default_item = models.ForeignKey(
        Item,
        blank=True,
        null=True,
        related_name="menu_default",
        on_delete=models.SET_NULL,
    )

    timeout = models.PositiveIntegerField(blank=True, null=True, help_text="in second")

    class Meta:
        ordering = ("title",)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse(
            "htmlmenu",
            kwargs={
                "pk": self.pk,
            },
        )

    def clean(self):
        if self.timeout is not None and not self.default_item:
            raise ValidationError(
                {"default_item": "Field 'timeout' is set but 'default_item' is not!"}
            )

    def get_ipxe_timeout(self):
        if self.timeout is None:
            return None
        return self.timeout * 1000

    def get_categorized_items(self, items=None):
        if items is None:
            items = self.get_items()
        return ItemCategory.categorize_items(items)

    def populate_item_cache(self):
        if not hasattr(self, "item_cache"):
            self.item_cache = self.get_items()
            self.categorized_items = self.get_categorized_items(self.item_cache)

    def get_items(self, **kwargs):
        items = set(
            self.items.all()
            .filter(**kwargs)
            .select_related(
                "category",
                "submenu",
                "image",
            )
        )
        item_groups_ids = list(self.item_groups.all().values_list("pk", flat=True))
        items |= set(
            Item.objects.filter(
                models.Q(itemgroup__pk__in=item_groups_ids, **kwargs)
                | models.Q(category__itemgroup__pk__in=item_groups_ids, **kwargs)
            )
            .select_related(
                "category",
                "submenu",
                "image",
            )
            .distinct()
        )
        if self.default_item:
            items.add(self.default_item)
        ordered_items = []
        for i in items:
            ordered_items.append(
                (
                    i.category.priority if i.category else 0,
                    i.category.label if i.category else "",
                    i.priority,
                    i.label,
                    i,
                )
            )
        return list([i[4] for i in sorted(ordered_items)])


class MatchingGroup(models.Model):
    name = models.CharField(
        max_length=256,
        unique=True,
    )

    menu = models.ForeignKey(
        Menu,
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
    )

    begin_at = models.DateTimeField(blank=True, null=True)

    end_at = models.DateTimeField(blank=True, null=True)

    epitaUserAllowed = models.BooleanField(default=False)

    priority = models.SmallIntegerField(
        default=0,
        blank=True,
    )

    class Meta:
        indexes = [models.Index(fields=["begin_at", "end_at"])]
        ordering = ("priority", "name")

    def clean(self):
        if self.begin_at is not None and self.end_at is None:
            raise ValidationError(
                {
                    "end_at": "End date and time must be set if begin date and time is set."
                }
            )
        if self.end_at is not None and self.begin_at is None:
            raise ValidationError(
                {
                    "begin_at": "Begin date and time must be set if end date and time is set."
                }
            )
        if (
            self.begin_at is not None
            and self.end_at is not None
            and self.begin_at >= self.end_at
        ):
            raise ValidationError(
                {"end_at": "End date and time must be after begin date and time."}
            )

        return super().clean()

    def __str__(self):
        return self.name


class Matcher(models.Model):
    group = models.ForeignKey(
        MatchingGroup,
        on_delete=models.CASCADE,
    )

    class Meta:
        abstract = True

    @classmethod
    def match(cls, mac_address=None, ip_address=None, **kwargs):
        date_lookup = models.Q(begin_at__isnull=True, end_at__isnull=True) | models.Q(
            begin_at__lte=now(), end_at__gt=now()
        )
        param_lookup = models.Q()

        if mac_address is not None:
            param_lookup |= models.Q(macaddressmatcher__mac_address=mac_address)
        if ip_address is not None:
            param_lookup |= models.Q(ipaddressmatcher__ip_address=ip_address)
            param_lookup |= models.Q(
                fleetroommatcher__room__dhcp_subnet__subnet__net_contains=ip_address
            )
            param_lookup |= models.Q(
                id__in=RawSQL(
                    """
                    SELECT
                        "pxe_matchinggroup"."id"
                    FROM
                        "pxe_matchinggroup"
                        INNER JOIN "pxe_networkmatcher" ON ("pxe_matchinggroup"."id" = "pxe_networkmatcher"."group_id")
                    WHERE
                        set_masklen(pxe_networkmatcher.network_address, pxe_networkmatcher.prefix_length) >> %s::inet
                    """,
                    (ip_address,),
                )
            )

        return (
            MatchingGroup.objects.filter(date_lookup & param_lookup)
            .select_related(
                "menu",
                "menu__default_item",
                "menu__default_item__category",
                "menu__default_item__submenu",
                "menu__default_item__image",
            )
            .order_by("priority")
            .first()
        )


class FleetRoomMatcher(Matcher):
    room = models.ForeignKey(fleet_models.Room, on_delete=models.CASCADE)

    def __str__(self):
        return self.room.name


class MacAddressMatcher(Matcher):
    mac_address = models.CharField(
        max_length=256,
    )

    def __str__(self):
        return self.mac_address


class IPAddressMatcher(Matcher):
    ip_address = models.GenericIPAddressField(unpack_ipv4=True, db_index=True)

    class Meta:
        verbose_name = "IP address matcher"

    def __str__(self):
        return self.ip_address


class NetworkMatcher(Matcher):
    network_address = models.GenericIPAddressField(unpack_ipv4=True, db_index=True)

    prefix_length = models.PositiveSmallIntegerField()

    def to_python(self):
        return ip_network((self.network_address, self.prefix_length))

    def clean(self):
        try:
            self.to_python()
        except ValueError as e:
            raise ValidationError("Invalid network specified") from e

    def __str__(self):
        return str(self.to_python())
