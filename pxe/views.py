import logging
from itertools import chain

from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views import View
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import DetailView, TemplateView

from . import models


logger = logging.getLogger(__name__)


class IndexView(TemplateView):
    template_name = "menu/index.ipxe"


class MenuMixin:
    # pylint:disable=unused-argument
    def get_submenus(self, menu):
        return []


class MenuView(MenuMixin, TemplateView):
    template_name = "menu/menu.ipxe"

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        self.parameters = {"ip_address": self.request.META["REMOTE_ADDR"]}
        self.menu_cache = {}

        matching_group = self.get_matching_menu()
        if matching_group is not None:
            self.matching_menu = matching_group.menu
            self.menu_cache[self.matching_menu.id] = self.matching_menu
            self.matching_menu.populate_item_cache()

        logger.info(
            'view=MenuView ip=%s matched_matching_group="%s"',
            self.request.META["REMOTE_ADDR"],
            matching_group,
        )

        return super().dispatch(*args, **kwargs)

    def post(self, *args, **kwargs):
        self.parameters.update(dict(self.request.POST.items()))
        return self.get(*args, **kwargs)

    def get_context_data(self, *args, **kwargs):
        c = super().get_context_data(*args, **kwargs)

        c["menu"] = self.matching_menu
        c["submenus"] = set(self.get_submenus(self.matching_menu))
        c["items"] = set(
            chain(
                self.matching_menu.item_cache,
                *[sm.item_cache for sm in c["submenus"]],
            )
        )

        return c

    def get_matching_menu(self):
        return models.Matcher.match(**self.parameters)

    def get_submenus(self, menu):
        submenus = set()
        remaining = set()
        remaining.add(menu)
        while remaining:
            m = remaining.pop()
            submenus.add(m)
            for item in m.item_cache:
                if (
                    item.type == "submenu"
                    and item.submenu is not None
                    and item.submenu not in submenus
                ):
                    submenu = self.menu_cache.get(item.submenu.id, item.submenu)
                    self.menu_cache[submenu.id] = submenu
                    submenu.populate_item_cache()
                    remaining.add(submenu)
        return submenus


class HTMLMenuView(MenuMixin, DetailView):
    model = models.Menu
    template_name = "menu/menu.html"
    context_object_name = "menu"

    def get_object(self, queryset=None):
        if hasattr(self, "object"):
            return self.object

        if "pk" in self.kwargs:
            self.object = super().get_object(queryset=queryset)
            self.object.populate_item_cache()
            return self.object

        self.matching_menus = self.get_matching_menus()
        if len(self.matching_menus) == 1:
            self.object = self.matching_menus.pop()
            self.object.populate_item_cache()
        elif len(self.matching_menus) > 1:
            self.object = models.Menu(
                pk=-1,
                title="Multiple menus available",
            )
            self.object.categorized_items = [
                models.Item(label=m.title, type="submenu", submenu=m)
                for m in self.matching_menus
            ]
        else:
            self.object = models.Menu(
                pk=-2,
                title="No menu available",
            )
            self.object.categorized_items = [
                models.Item(
                    label=self.object.title,
                    type="separator",
                ),
            ]
        return self.object

    def get_matching_menus(self):
        return models.Menu.objects.all()

    def get_back_url(self):
        if len(self.backstack) < 3:
            return reverse(
                "htmlmenu",
                kwargs={
                    "pk": self.backstack[-1].pk,
                },
            )
        return reverse(
            "htmlmenu",
            kwargs={
                "pk": self.backstack[-1].pk,
                "backstack": ",".join(
                    map(lambda m: str(m.id), self.backstack[1:-1]),
                ),
            },
        )

    def get_backstack(self):
        return ",".join(
            map(
                lambda m: str(m.id),
                self.backstack[1:] + [self.get_object()],
            )
        )

    def get(self, *args, backstack=None, **kwargs):
        self.backstack = [self.get_object()]
        if backstack is not None:
            for menu_id in backstack.split(","):
                try:
                    menu_id = int(menu_id)
                except ValueError:
                    break
                if menu_id < 0:
                    continue
                self.backstack.append(get_object_or_404(models.Menu, id=menu_id))
        return super().get(*args, **kwargs)

    def get_context_data(self, *args, **kwargs):
        c = super().get_context_data(*args, **kwargs)
        c["view"] = self
        return c


class CheckEpitaUserAllowed(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        self.parameters = {"ip_address": self.request.META["REMOTE_ADDR"]}
        return super().dispatch(*args, **kwargs)

    def get(self, *args, **kwargs):
        group = models.Matcher.match(**self.parameters)
        allowed = group is None or (group is not None and group.epitaUserAllowed)
        logger.info(
            'view=CheckEpitaUserAllowed ip=%s matched_matching_group="%s" anonymous_allowed=%s',
            self.request.META["REMOTE_ADDR"],
            group,
            allowed,
        )
        if not allowed:
            return HttpResponse(
                status=403,
            )
        return HttpResponse(
            status=200,
        )
