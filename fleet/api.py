from django.urls import include, path
from rest_framework.routers import DefaultRouter

from .views import (
    DhcpConfigDiscovery,
    IssueViewSet,
    MachineViewSet,
    PrometheusServiceDiscovery,
    Register,
    Registered,
    RoomViewSet,
    SiteViewSet,
    WorkstationViewSet,
)

router = DefaultRouter()
router.root_view_name = "fleet-api-root"
router.register("sites", SiteViewSet)
router.register("rooms", RoomViewSet)
router.register("workstations", WorkstationViewSet)
router.register("machines", MachineViewSet)
router.register("issues", IssueViewSet)

urlpatterns = [
    path("", include(router.urls)),
    path(
        "dhcp_config_discovery/",
        DhcpConfigDiscovery.as_view(),
        name="fleet_dhcp_config_discovery",
    ),
    path(
        "prometheus_sd/",
        PrometheusServiceDiscovery.as_view(),
        name="fleet_prometheus_sd",
    ),
    path("register/", Register.as_view(), name="fleet_register"),
    path("registered/", Registered.as_view(), name="fleet_registered"),
]
