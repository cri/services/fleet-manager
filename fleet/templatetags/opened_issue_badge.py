from django import template

from fleet.models import Issue

register = template.Library()


@register.inclusion_tag("fleet/opened-issue-badge.html")
def opened_issue_badge(user):
    return (
        {
            "opened_issue_count": Issue.objects.filter(status=Issue.Status.NEW).count(),
        }
        if user.has_perm("fleet.view_issue")
        else {}
    )
