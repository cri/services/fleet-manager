import logging
from xml.etree import ElementTree

from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin, messages
from django.core.exceptions import ValidationError
from django.http import (
    Http404,
    HttpResponseForbidden,
    HttpResponseRedirect,
    JsonResponse,
)
from django.shortcuts import redirect, render
from django.template import Context, Engine
from django.urls import reverse, reverse_lazy
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import (
    CreateView,
    DetailView,
    ListView,
    TemplateView,
    UpdateView,
    View,
)
from django.views.generic.detail import SingleObjectMixin
from django.views.generic.edit import ModelFormMixin
from django_filters import filters
from django_filters.rest_framework import FilterSet
from rest_framework.decorators import action
from rest_framework.permissions import DjangoModelPermissionsOrAnonReadOnly
from rest_framework.viewsets import ModelViewSet

from sessions_state.models import Session, SessionState

from .filters import IssueFilter
from .forms import IssueCreateForm, IssueForm, RegisterForm
from .models import DhcpSubnet, Issue, Machine, Room, Site, Workstation
from .permissions import DjangoModelWithViewPermissions
from .serializers import (
    IssueSerializer,
    MachineSerializer,
    RoomSerializer,
    SiteSerializer,
    WorkstationSerializer,
)

logger = logging.getLogger(__name__)


class DhcpConfigDiscovery(View):
    http_method_names = ("get",)

    def get(self, request, *args, **kwargs):
        if (
            "Kea-Shared-Secret" not in request.headers
            or request.headers["Kea-Shared-Secret"] != settings.KEA_SHARED_SECRET
        ):
            return HttpResponseForbidden("Kea-Shared-Secret missing or invalid.")

        dhcp_config = {
            "Dhcp4": {
                "valid-lifetime": settings.DHCP_VALID_LIFETIME,
                "min-valid-lifetime": settings.DHCP_MIN_VALID_LIFETIME,
                "max-valid-lifetime": settings.DHCP_MAX_VALID_LIFETIME,
                "match-client-id": False,
                "calculate-tee-times": True,
                "control-socket": {
                    "socket-type": "unix",
                    "socket-name": settings.DHCP_CONTROL_SOCKET_PATH,
                },
                "interfaces-config": {
                    "interfaces": ["*"],
                    "dhcp-socket-type": "udp",
                    "outbound-interface": "use-routing",
                },
                "option-def": [
                    {
                        "name": "no-pxedhcp",
                        "code": 176,
                        "type": "uint8",
                        "array": False,
                        "record-types": "",
                        "space": "ipxe",
                        "encapsulate": "",
                    },
                    {
                        "name": "https",
                        "code": 20,
                        "type": "uint8",
                        "array": False,
                        "record-types": "",
                        "space": "ipxe",
                        "encapsulate": "",
                    },
                ],
                "option-data": [
                    {
                        "name": "domain-name-servers",
                        "data": ", ".join(settings.DHCP_DOMAIN_NAME_SERVERS),
                        "always-send": True,
                    },
                    {
                        "name": "ntp-servers",
                        "data": ", ".join(settings.DHCP_NTP_SERVERS),
                        "always-send": True,
                    },
                    {
                        "name": "no-pxedhcp",
                        "code": 176,
                        "space": "ipxe",
                        "data": "1",
                        "always-send": True,
                    },
                ],
                "next-server": settings.DHCP_NEXT_SERVER,
                "client-classes": [
                    {
                        "name": "ipxe_efi_x86",
                        "test": "option[client-system].hex == 0x0006",
                        "boot-file-name": "ipxe.efi",
                    },
                    {
                        "name": "ipxe_efi_x64",
                        "test": "option[client-system].hex == 0x0007",
                        "boot-file-name": "ipxe.efi",
                    },
                    {
                        "name": "ipxe_efi_x64_x86",
                        "test": "option[client-system].hex == 0x0009",
                        "boot-file-name": "ipxe.efi",
                    },
                    {
                        "name": "ipxe_legacy_x64",
                        "test": "not option[client-system].hex == 0x0006 and not option[client-system].hex == 0x0007 and not option[client-system].hex == 0x0009",  # noqa: E501
                        "boot-file-name": "undionly.kpxe",
                    },
                ],
                "subnet4": list(
                    map(
                        lambda subnet: {
                            "id": subnet.id,
                            "subnet": str(subnet.subnet),
                            "pools": [
                                {
                                    "pool": f"{subnet.allocation_pool_start} - {subnet.allocation_pool_end}"  # noqa: E501
                                }
                            ],
                            "option-data": [
                                {
                                    "name": "domain-name",
                                    "data": (
                                        ""
                                        if subnet.room_set.count() == 0
                                        else (
                                            subnet.room_set.first().domain
                                            if subnet.room_set.count() == 1
                                            else subnet.room_set.first().site.domain
                                        )
                                    ),
                                },
                                {
                                    "name": "routers",
                                    "data": str(subnet.router),
                                },
                            ],
                            "reservations": list(
                                map(
                                    lambda w: {
                                        "hw-address": str(w.machine.mac),
                                        "ip-address": str(w.ip),
                                        "hostname": w.fqdn,
                                    },
                                    Workstation.objects.filter(
                                        room__in=Room.objects.filter(
                                            dhcp_subnet=subnet
                                        ),
                                        machine__isnull=False,
                                    ).select_related("machine", "room"),
                                )
                            ),
                        },
                        DhcpSubnet.objects.all(),
                    )
                ),
                "loggers": [
                    {
                        "name": "kea-dhcp4",
                        "output_options": [
                            {
                                "output": "stdout",
                                "pattern": "%-5p %m\n",
                            },
                        ],
                        "severity": settings.DHCP_LOG_LEVEL,
                        "debuglevel": settings.DHCP_DEBUG_LEVEL,
                    },
                ],
            }
        }

        return JsonResponse(dhcp_config)


class PrometheusServiceDiscovery(View):
    http_method_names = ("get",)

    def get(self, request, *args, **kwargs):
        return JsonResponse(
            list(
                map(
                    lambda workstation: {
                        "targets": [f"{workstation.ip}:9100"],
                        "labels": {
                            "hostname": workstation.name,
                            "room": workstation.room.slug,
                            "site": workstation.room.site.domain,
                        },
                    },
                    Workstation.objects.select_related("room", "room__site")
                    .order_by()
                    .all(),
                )
            ),
            safe=False,
        )


class Registered(TemplateView):
    template_name = "fleet/registered.ipxe"


class Register(TemplateView):
    form_class = RegisterForm
    template_name = "fleet/register.ipxe"
    success_url = reverse_lazy("fleet_registered")

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)

        try:
            room = Room.objects.get(currently_mapping=True)
        except Room.DoesNotExist:
            form.add_error(None, ValidationError("No room is currently mapping."))

        if not form.is_valid():
            return render(request, self.template_name, {"form": form})

        try:
            workstation = Workstation.objects.get(
                name=form.cleaned_data["name"], room=room
            )
            workstation.switch_name = form.cleaned_data["switch_name"]
            workstation.switch_port = form.cleaned_data["switch_port"]
        except Workstation.DoesNotExist:
            workstation = form.save(commit=False)
        workstation.room = room
        workstation.save()

        if hasattr(workstation, "machine"):
            old_machine = workstation.machine
            old_machine.workstation = None
            old_machine.save()

        Machine.objects.update_or_create(
            mac=form.cleaned_data["mac"],
            defaults={
                "workstation": workstation,
                "board_uuid": form.cleaned_data["board_uuid"],
                "board_serial": form.cleaned_data["board_serial"],
            },
        )

        return HttpResponseRedirect(self.success_url)


class SiteViewSet(ModelViewSet):
    queryset = Site.objects.all()
    serializer_class = SiteSerializer
    permission_classes = [DjangoModelWithViewPermissions]


class RoomViewSet(ModelViewSet):
    queryset = Room.objects.all()
    serializer_class = RoomSerializer
    permission_classes = [DjangoModelWithViewPermissions]
    filterset_fields = ("site",)


class WorkstationFilter(FilterSet):
    ip = filters.CharFilter(field_name="ip", lookup_expr="exact")

    class Meta:
        model = Workstation
        fields = ["room", "ip"]


class WorkstationViewSet(ModelViewSet):
    queryset = Workstation.objects.all()
    serializer_class = WorkstationSerializer
    permission_classes = [DjangoModelWithViewPermissions]
    filterset_class = WorkstationFilter


class MachineViewSet(ModelViewSet):
    queryset = Machine.objects.all()
    serializer_class = MachineSerializer
    permission_classes = [DjangoModelWithViewPermissions]


class IssueViewSet(ModelViewSet):
    queryset = Issue.objects.all()
    serializer_class = IssueSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    filterset_class = IssueFilter

    @action(detail=True, methods=["patch"])
    def reject(self, request, *args, **kwargs):
        self.get_object().reject(request.user, save=True)
        return self.retrieve(request)

    @action(detail=True, methods=["patch"])
    def resolve(self, request, *args, **kwargs):
        self.get_object().resolve(request.user, save=True)
        return self.retrieve(request)


class IssueListView(LoginRequiredMixin, ListView):
    template_name = "fleet/issue-list.html"
    queryset = Issue.objects.order_by("-added_at").select_related(
        "added_by", "workstation", "workstation__room", "workstation__room__site"
    )
    paginate_by = 20
    filterset_class = IssueFilter

    def dispatch(self, request, *args, **kwargs):
        if (
            not request.user.is_staff
            and self.kwargs.get("username") != request.user.username
        ):
            return self.handle_no_permission()

        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        qs = super().get_queryset()
        if "username" in self.kwargs:
            qs = qs.filter(added_by__username=self.kwargs["username"])
        self.filterset = self.filterset_class(self.request.GET, queryset=qs)
        return self.filterset.qs

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(object_list=object_list, **kwargs)
        page = context["page_obj"]
        context["paginator_range"] = page.paginator.get_elided_page_range(page.number)
        if "username" in self.kwargs:
            context["profile_username"] = self.kwargs["username"]
            context["user_profile_page"] = "my_issues"
        context["issues_display_workstation"] = True
        f = IssueFilter(self.request.GET, queryset=self.get_queryset())
        context["filter"] = f
        return context


class SessionListView(LoginRequiredMixin, ListView):
    template_name = "fleet/session-list.html"
    queryset = Session.objects.all()
    paginate_by = 20

    def dispatch(self, request, *args, **kwargs):
        if not self.request.user.has_perm("sessions_state.view_session"):
            return self.handle_no_permission()

        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        qs = super().get_queryset()
        login = self.request.GET.get("login")
        exact = self.request.GET.get("exact")
        ip = self.request.GET.get("ip")
        if login:
            if exact == "on":
                qs = qs.filter(login=login)
            else:
                qs = qs.filter(login__contains=login)
        if ip:
            if exact == "on":
                qs = qs.filter(ip=ip)
            else:
                qs = qs.filter(ip__contains=ip)
        return qs

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(object_list=object_list, **kwargs)
        page = context["page_obj"]
        context["paginator_range"] = page.paginator.get_elided_page_range(page.number)
        login = self.request.GET.get("login")
        if login:
            context["sessions_login"] = login
        context["session_display_ip"] = True
        context["session_display_login"] = True
        return context


class RoomMapView(LoginRequiredMixin, DetailView):
    model = Room
    template_name = "fleet/room-map.html"

    @classmethod
    def __get_workstation_color(cls, workstation_sessions, workstation_issues):
        color = "var(--bs-success)"
        if workstation_sessions:
            color = "var(--bs-primary)"
            if workstation_sessions[-1].locked:
                color = "var(--bs-primary-bg-subtle)"
            return color

        if workstation_issues:
            issue_red = False
            for i in workstation_issues:
                if i.severity == Issue.Severity.HIGH:
                    issue_red = True
                    break  # worst case scenario, no need to go further
            color = "var(--bs-danger)" if issue_red else "var(--bs-warning)"

        return color

    @classmethod
    def __get_workstation_popover_content(
        cls, workstation_sessions, workstation_issues, admin_view
    ):
        engine = Engine.get_default()
        template = engine.get_template("fleet/workstation-map-popover.html")
        template_context = Context(
            {
                "sessions": workstation_sessions,
                "issues": workstation_issues,
                "admin_view": admin_view,
            }
        )
        return template.render(template_context)

    def generate_svg(self, admin_view: bool, heatmap: bool):
        room_domain = self.room.domain
        workstations = Workstation.objects.filter(room=self.room).select_related(
            "room", "room__site"
        )
        if heatmap:
            workstations_usage = self.room.get_workstation_usage_ratio()
        try:
            ElementTree.register_namespace("", "http://www.w3.org/2000/svg")
            root = ElementTree.fromstring(self.room.map)
        except ElementTree.ParseError:
            logger.error("%s has invalid SVG map", room_domain)
            return None
        for w in workstations:
            logger.debug("Handling %s map icon", w.fqdn)
            svg_node = root.find(f".//*[@id='{w.fqdn}']")
            if svg_node is None:
                logger.warning("%s is missing from the map", w.fqdn)
                continue
            parent_node = root.find(f".//*[@id='{w.fqdn}']/..")
            parent_node.remove(svg_node)
            link_node = ElementTree.SubElement(
                parent_node,
                "a",
                attrib={"href": reverse("workstation_detail", kwargs={"pk": w.id})},
            )
            link_node.append(svg_node)
            workstation_sessions = list(
                filter(lambda s: s.ip == str(w.ip), self.room_current_sessions)
            )
            workstation_issues = list(
                filter(lambda i: i.workstation == w, self.room_issues)
            )
            if heatmap:
                usage = next(
                    (x.used_ratio for x in workstations_usage if x.id == w.id), 0.0
                )
                color = f"hsl({int((1. - usage) * 240.)}, 100%, 50%)"
            else:
                color = self.__get_workstation_color(
                    workstation_sessions, workstation_issues
                )
            svg_style = svg_node.get("style") or ""
            if svg_style:
                svg_style += ";"
            svg_style += f"fill: {color};"
            svg_node.set("style", svg_style)
            svg_node.set("data-bs-toggle", "popover")
            svg_node.set("data-bs-title", w.name)
            svg_node.set(
                "data-bs-content",
                self.__get_workstation_popover_content(
                    workstation_sessions, workstation_issues, admin_view
                ),
            )
        return ElementTree.tostring(root).decode("ascii")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        self.room = self.get_object()
        heatmap = self.request.GET.get("heatmap") == "1"

        # Optimization: I put all necessary room info in memory instead of
        # querying the database for each workstation in the room.
        # It saves a tremendous amount of time.
        self.room_current_sessions: list[SessionState] = list(
            SessionState.objects.by_room(self.room)
            .current()
            .order_by("login", "timestamp")
        )
        self.room_issues: list[Issue] = list(
            Issue.objects.filter(
                workstation__room=self.room,
                status=Issue.Status.NEW,
            )
        )

        context["svg"] = self.generate_svg(
            self.request.user.is_staff,
            heatmap,
        )
        context["issue_count"] = len(self.room_issues)
        context["used_machines"] = len({s.ip: 0 for s in self.room_current_sessions})
        return context


class MapsView(LoginRequiredMixin, TemplateView):
    template_name = "fleet/maps.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        rooms = (
            Room.objects.filter(show_in_frontend=True)
            .select_related("site")
            .order_by("site", "slug")
        )
        context["rooms"] = rooms

        heatmap = self.request.GET.get("heatmap") == "on"
        context["heatmap"] = heatmap

        selected_rooms_slugs = self.request.GET.getlist("room")
        selected_rooms = Room.objects.filter(slug__in=selected_rooms_slugs)

        context["selected_rooms"] = selected_rooms
        context["selected_rooms_slugs"] = selected_rooms_slugs

        return context


class UserSessionHistoryView(LoginRequiredMixin, ListView):
    template_name = "fleet/usersessionhistory-list.html"
    model = Session
    paginate_by = 20

    def dispatch(self, request, *args, **kwargs):
        if (
            not request.user.is_staff
            and request.user.username != self.kwargs["username"]
        ):
            return self.handle_no_permission()

        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        return super().get_queryset().filter(login=self.kwargs["username"])

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(object_list=object_list, **kwargs)
        page = context["page_obj"]
        context["paginator_range"] = page.paginator.get_elided_page_range(page.number)
        context["sessions_username"] = self.kwargs["username"]
        context["profile_username"] = self.kwargs["username"]
        context["user_profile_page"] = "session_history"
        context["session_display_ip"] = True
        return context


class UserProfileView(LoginRequiredMixin, DetailView):
    model = get_user_model()
    template_name = "fleet/userprofile-detail.html"

    def get_object(self, queryset=None):
        username = self.kwargs["username"]
        qs = queryset or self.get_queryset()
        user = qs.filter(username=username).first()
        # Create a fake user if the user never logged to fleet but has sessions
        if not user and Session.objects.filter(login=username).count():
            user = self.model(username=username)
        if not user:
            raise Http404(f"User {username} not found")
        return user

    def get_context_object_name(self, obj):
        return "profile_user"

    @staticmethod
    def __get_sessions(sessions_states):
        sessions = {}
        for s in sessions_states:
            session = sessions.get(s.ip)
            if session is None:
                sessions[s.ip] = {
                    "image": s.image,
                    "locked": s.locked,
                    "start": s.timestamp,
                    "latest": s.timestamp,
                    "workstation": Workstation.objects.filter(ip=s.ip).first(),
                }
            else:
                if session["start"] > s.timestamp:
                    session["start"] = s.timestamp
                if session["latest"] < s.timestamp:
                    session["locked"] = s.locked
                    session["latest"] = s.timestamp
        return sessions

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        user = self.get_object()
        if self.request.user.is_staff or self.request.user == user:
            sessions_states = SessionState.objects.filter(login=user.username)
            context["sessions"] = self.__get_sessions(sessions_states)
        context["profile_username"] = user.username
        context["user_profile_page"] = "profile"
        context["reported_issues"] = Issue.objects.filter(added_by=user).count()
        context["resolved_issues"] = Issue.objects.filter(
            added_by=user, status=Issue.Status.RESOLVED
        ).count()
        return context


class WorkstationDetailView(LoginRequiredMixin, DetailView):
    model = Workstation
    template_name = "fleet/workstation-detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        workstation = self.get_object()
        context["issue_list"] = Issue.objects.filter(
            workstation=workstation,
            status=Issue.Status.NEW,
        )
        if self.request.user.has_perm("sessions_state.view_session"):
            context["session_list"] = Session.objects.filter(ip=workstation.ip)[:5]
            context["session_display_login"] = True
            context["current_sessions"] = list(
                SessionState.objects.filter(ip=workstation.ip).order_by(
                    "login", "timestamp"
                )
            )
        return context


class WorkstationIssueFormMixin(
    LoginRequiredMixin, SuccessMessageMixin, ModelFormMixin
):
    template_name = "fleet/issue-form.html"
    form_class = IssueCreateForm
    workstation = None
    model = Issue

    def get_workstation(self):
        if not self.workstation:
            self.workstation = Workstation.objects.get(pk=self.kwargs["workstation_pk"])
        return self.workstation

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["workstation"] = self.get_workstation()
        return kwargs

    def get_success_url(self):
        return reverse("workstation_detail", kwargs={"pk": self.get_workstation().id})

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        workstation = self.get_workstation()
        context["workstation"] = workstation
        return context


class WorkstationIssueCreateFormView(WorkstationIssueFormMixin, CreateView):
    success_message = "The issue was sucessfully created."

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["creator"] = self.request.user
        return kwargs


class WorkstationIssueUpdateFormView(WorkstationIssueFormMixin, UpdateView):
    form_class = IssueForm
    success_message = "The issue was sucessfully updated."

    def get_success_url(self):
        previous = self.request.META.get("HTTP_REFERER")
        return previous or reverse(
            "workstation_detail", kwargs={"pk": self.get_workstation().id}
        )

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_staff:
            return self.handle_no_permission()

        return super().dispatch(request, *args, **kwargs)


class WorkstationIssueActionView(LoginRequiredMixin, SingleObjectMixin, View):
    model = Issue

    # To implement: returns action to perform on issue
    def run_action(self, request, issue):
        pass

    def get(self, request, *args, **kwargs):
        if not request.user.is_staff:
            return self.handle_no_permission()
        issue = self.get_object()
        self.run_action(request, issue)
        messages.success(request, self.action_success_message)
        previous = request.META.get("HTTP_REFERER")
        if previous:
            return redirect(previous)
        return redirect("workstation_detail", pk=issue.workstation.id)


class WorkstationIssueRejectView(WorkstationIssueActionView):
    action_success_message = "Issue rejected!"

    def run_action(self, request, issue):
        issue.reject(request.user, save=True)


class WorkstationIssueResolveView(WorkstationIssueActionView):
    action_success_message = "Issue resolved!"

    def run_action(self, request, issue):
        issue.resolve(request.user, save=True)
