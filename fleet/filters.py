import django_filters
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Column, Field, Layout, Row, Submit
from django_filters.filters import forms

from fleet.models import Issue, Room, Site


class IssueFilterForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = "get"
        self.helper.form_class = "container"
        self.helper.layout = Layout(
            Row(
                Column(Field("status")),
                Column(Field("severity")),
                Column(Field("problem_type")),
            ),
            Row(
                Column(Field("site")),
                Column(Field("room")),
                Column(Field("workstation")),
            ),
            Submit("submit", "Search"),
        )


class IssueFilter(django_filters.FilterSet):
    status = django_filters.MultipleChoiceFilter(choices=Issue.Status.choices)
    severity = django_filters.MultipleChoiceFilter(choices=Issue.Severity.choices)
    problem_type = django_filters.MultipleChoiceFilter(
        choices=Issue.ProblemType.choices
    )
    ip = django_filters.CharFilter(field_name="workstation__ip")
    site = django_filters.ModelMultipleChoiceFilter(
        field_name="workstation__room__site", queryset=Site.objects.all()
    )
    room = django_filters.ModelMultipleChoiceFilter(
        field_name="workstation__room", queryset=Room.objects.all()
    )
    workstation = django_filters.CharFilter(field_name="workstation__name")

    class Meta:
        model = Issue
        fields = [
            "status",
            "severity",
            "problem_type",
            "site",
            "room",
            "workstation",
            "ip",
        ]
        form = IssueFilterForm
