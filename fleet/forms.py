from django import forms

from .models import Issue, Workstation


class RegisterForm(forms.ModelForm):
    mac = forms.CharField(min_length=17, max_length=17)

    board_uuid = forms.UUIDField(required=False)
    board_serial = forms.CharField(max_length=255, required=False)

    class Meta:
        model = Workstation
        fields = (
            "name",
            "switch_name",
            "switch_port",
            "mac",
            "board_uuid",
            "board_serial",
        )

    # We check this manually in the Register view
    def validate_unique(self):
        pass


class IssueForm(forms.ModelForm):
    class Meta:
        model = Issue
        fields = ["problem_type", "severity", "description", "status"]

    def __init__(self, *args, workstation, **kwargs):
        super().__init__(*args, **kwargs)
        self.workstation = workstation

    def save(self, commit=True):
        issue = super().save(commit=False)
        issue.workstation = self.workstation
        if commit:
            issue.save()
        return issue


class IssueCreateForm(IssueForm):
    class Meta(IssueForm.Meta):
        fields = ["problem_type", "severity", "description"]

    def __init__(self, *args, creator, **kwargs):
        super().__init__(*args, **kwargs)
        self.creator = creator

    def save(self, commit=True):
        issue = super().save(commit=False)
        issue.added_by = self.creator
        if commit:
            issue.save()
        return issue
