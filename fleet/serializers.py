from rest_framework import serializers

from .models import Issue, Machine, Room, Site, Workstation


class IssueSerializer(serializers.HyperlinkedModelSerializer):
    added_by = serializers.HiddenField(default=serializers.CurrentUserDefault())

    class Meta:
        model = Issue
        fields = (
            "url",
            "id",
            "workstation",
            "description",
            "problem_type",
            "severity",
            "status",
            "added_by",
        )


class MachineSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Machine
        # We need to overwrite fields to include id
        fields = (
            "url",
            "id",
            "mac",
            "board_uuid",
            "board_serial",
            "added_at",
            "workstation",
        )


class WorkstationSerializer(serializers.HyperlinkedModelSerializer):
    room = serializers.PrimaryKeyRelatedField(queryset=Room.objects.all())

    machine = MachineSerializer(read_only=True)

    issues = serializers.SerializerMethodField()

    def get_issues(self, instance):
        return IssueSerializer(
            instance.issue_set.exclude(status=Issue.Status.RESOLVED).exclude(
                status=Issue.Status.REJECTED
            ),
            context=self.context,
            many=True,
        ).data

    class Meta:
        model = Workstation
        # We need to overwrite fields to include id
        fields = (
            "url",
            "id",
            "name",
            "fqdn",
            "room",
            "ip",
            "switch_name",
            "switch_port",
            "added_at",
            "exclude_from_mapping",
            "machine",
            "issues",
        )


class RoomSerializer(serializers.HyperlinkedModelSerializer):
    site = serializers.PrimaryKeyRelatedField(queryset=Site.objects.all())

    class Meta:
        model = Room
        # We need to overwrite fields to include id
        fields = (
            "url",
            "id",
            "name",
            "slug",
            "site",
            "mapping_network",
            "currently_mapping",
            "map",
            "domain",
        )


class SiteSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Site
        # We need to overwrite fields to include id
        fields = (
            "url",
            "id",
            "name",
            "domain",
        )
