from django.conf import settings
from django.utils import timezone
from modelsync import SyncedModelAdapter, UnidirectionalSyncEngine
from modelsync.modelengine import RemoteModelSyncMixin

from powerdns import models as pdns_models


class DomainSyncRemoteModelEngine(RemoteModelSyncMixin, UnidirectionalSyncEngine):
    def get_remote_obj(self, remote_obj_id):
        return self.remote_model.objects.get(name=remote_obj_id)


class DomainSyncAdapter(SyncedModelAdapter):
    sync_engine_class = DomainSyncRemoteModelEngine
    sync_remote_model = pdns_models.Domain

    def get_sync_local_obj_id(self):
        return self.obj.domain

    def get_sync_remote_obj_id(self, _remote_objects):
        return self.obj.domain

    def get_sync_local_data(self):
        data = {
            "name": self.obj.domain,
            "type": "NATIVE",
        }
        return self.obj.domain, data

    @staticmethod
    def get_sync_remote_data(remote_obj):
        SYNC_FIELDS = (
            "name",
            "type",
        )
        data = {
            **{f: getattr(remote_obj, f) for f in SYNC_FIELDS},
        }
        return remote_obj.name, data

    @classmethod
    # pylint:disable=arguments-renamed
    def sync_all(cls, models, *, pretend=False):
        objs = []
        for model in models:
            objs += list(map(cls, cls.get_sync_local_queryset(model)))
        return cls._sync(objs, pretend=pretend, partial=False)


class RecordTypeRemoteModelEngine(RemoteModelSyncMixin, UnidirectionalSyncEngine):
    def get_remote_obj(self, remote_obj_id):
        return self.remote_model.objects.get(name=remote_obj_id, type=self.type)


class RecordSOARemoteModelEngine(RecordTypeRemoteModelEngine):
    type = "SOA"


class RecordNSRemoteModelEngine(RecordTypeRemoteModelEngine):
    type = "NS"


class RecordARemoteModelEngine(RecordTypeRemoteModelEngine):
    type = "A"


class RecordTXTRemoteModelEngine(RecordTypeRemoteModelEngine):
    type = "TXT"


class RecordTypeSyncAdapter(SyncedModelAdapter):
    sync_remote_model = pdns_models.Record

    @classmethod
    def get_sync_remote_queryset(cls):
        return cls.sync_remote_model.objects.filter(type=cls.type)

    def get_sync_local_obj_id(self):
        return self.obj.domain

    def get_sync_remote_obj_id(self, _remote_objects):
        return self.obj.domain

    def get_sync_local_data(self):
        data = {
            "domain": pdns_models.Domain.objects.get(name=self.obj.zone),
            "name": self.obj.domain,
            "type": self.type,
            "content": self.get_content(),
            "ttl": settings.DNS_RECORD_DEFAULT_TTL,
            "disabled": False,
            "auth": False,
        }
        return self.obj.domain, data

    @staticmethod
    def get_sync_remote_data(remote_obj):
        SYNC_FIELDS = (
            "domain",
            "name",
            "type",
            "content",
            "ttl",
            "disabled",
            "auth",
        )
        data = {
            **{f: getattr(remote_obj, f) for f in SYNC_FIELDS},
        }
        return remote_obj.name, data

    @classmethod
    # pylint:disable=arguments-renamed
    def sync_all(cls, models, *, pretend=False):
        objs = []
        for model in models:
            objs += list(map(cls, cls.get_sync_local_queryset(model)))
        return cls._sync(objs, pretend=pretend, partial=False)


class RecordSOASyncAdapter(RecordTypeSyncAdapter):
    sync_engine_class = RecordSOARemoteModelEngine
    type = "SOA"

    def get_content(self):
        mname = settings.DNS_SOA_MNAME
        rname = settings.DNS_SOA_RNAME or f"hostmaster.{self.obj.domain}"
        serial = int(timezone.now().timestamp())
        refresh = settings.DNS_SOA_REFRESH
        retry = settings.DNS_SOA_RETRY
        expire = settings.DNS_SOA_EXPIRE
        ttl = settings.DNS_SOA_TTL

        return f"{mname} {rname} {serial} {refresh} {retry} {expire} {ttl}"


class RecordNSSyncAdapter(RecordTypeSyncAdapter):
    sync_engine_class = RecordNSRemoteModelEngine
    type = "NS"

    def get_content(self):
        return settings.DNS_NS_NAME


class RecordASyncAdapter(RecordTypeSyncAdapter):
    sync_engine_class = RecordARemoteModelEngine
    type = "A"

    def get_content(self):
        return str(self.obj.ip)


class RecordTXTSyncAdapter(RecordTypeSyncAdapter):
    sync_engine_class = RecordTXTRemoteModelEngine
    type = "TXT"

    def get_content(self):
        return self.obj.content
