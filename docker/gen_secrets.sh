#!/bin/sh

RAND_SECRET_NAMES="django-secret-key postgres-passwd postgres-powerdns-passwd postgres-salt-passwd kea-shared-secret salt-api-shared-secret"
USER_SECRET_NAMES="epita-openid-secret"
RSA_KEYPAIR_NAMES="salt-master"

for name in ${RAND_SECRET_NAMES}; do
	DST=./secrets/$name
	if [ ! -e "${DST}" ]; then
		tr -dc 'a-zA-Z0-9' < /dev/urandom | fold -w 32 | head -n 1 \
			> "${DST}"
	fi
done

# The following secrets have hardcoded values and are not modifiable for now
echo accessKey1 > ./secrets/s3-access
echo verySecretKey1 > ./secrets/s3-secret

for name in ${USER_SECRET_NAMES}; do
	DST=./secrets/$name
	envname=$(echo $name | sed 's/-/_/g')
	envvalue=$(eval echo \${$envname})
	if [ ! -z "$envvalue" ]; then
		echo "$envvalue" > "${DST}"
		continue
	fi
	if [ ! -e "${DST}" ]; then
		echo -n "Please input a value for ${name}: "
		read value
		echo "${value}" > "${DST}"
	fi
done

for name in ${RSA_KEYPAIR_NAMES}; do
	DST=./secrets/$name
	openssl genrsa -out "${DST}.pem" 2048
	openssl rsa -in "${DST}.pem" -outform PEM -pubout -out "${DST}.pub"
done
