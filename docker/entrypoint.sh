#!/bin/sh

set -e

getsecret() {
  FILE_PATH="$(eval "echo -n \"\${${1}_FILE}\"")"
  if [ ! -z "$FILE_PATH" ]; then
    cat "$FILE_PATH"
  else
    eval "echo -n \"\${${1}}\""
  fi
}

configure_s3_bucket() {
  export AWS_ACCESS_KEY_ID="$(getsecret S3_ACCESS_KEY)"
  export AWS_SECRET_ACCESS_KEY="$(getsecret S3_SECRET_KEY)"
  echo "Creating bucket ${S3_BUCKET}"
  aws --endpoint-url="${S3_ENDPOINT}" s3api create-bucket \
    --bucket="${S3_BUCKET}" --acl "public-read" || true
  echo "Adding CORS to bucket ${S3_BUCKET}"
  aws --endpoint-url="${S3_ENDPOINT}" s3api put-bucket-cors \
    --bucket="${S3_BUCKET}" \
    --cors-configuration="file://${CORS_CONFIG_PATH:-/app/cors.json}" || true
  echo "Adding policy to bucket ${S3_BUCKET}"
  aws --endpoint-url="${S3_ENDPOINT}" s3api put-bucket-policy \
        --bucket="${S3_BUCKET}" \
        --policy="$(sed "s/BUCKET/${S3_BUCKET}/" \
                 "${BUCKET_POLICY_PATH:-/app/bucket-policy.json}")" || true
  unset AWS_ACCESS_KEY_ID AWS_SECRET_ACCESS_KEY
}

init() {
  if [ -z "$SKIP_S3_CONFIGURATION" ]; then
    configure_s3_bucket
  fi

  ./manage.py collectstatic --noinput
  ./manage.py migrate --database=default
  ./manage.py migrate --database=powerdns
  ./manage.py migrate --database=salt

  if [ -n "$DEV" ] && [ "$#" -eq 0 ]; then
    echo Loading data...
    ./manage.py loaddata base || true
    ./manage.py loaddata dev || true
  fi
}

mkdir -p $HOME/.ipython/profile_default/
cp docker/config/ipython_config.py $HOME/.ipython/profile_default/

if [ -n "$INIT_JOB" ] || ([ -n "$DEV" ] && [ "$#" -eq 0 ]); then
  init
fi

if [ -n "$CHECK_DEPLOY" ] || ([ -z "$DEV" ] && [ "$#" -eq 0 ]); then
  ./manage.py check --deploy
fi

if [ "$#" -eq 0 ]; then
  if [ -n "$DEV" ]; then
    set -- ./manage.py runserver 0.0.0.0:8000
  else
    set -- gunicorn fleet_manager.wsgi -b 0.0.0.0:8000 --workers "${GUNICORN_WORKERS:-1}"
  fi
fi

exec "$@"
