# syntax = docker/dockerfile:experimental

FROM registry.cri.epita.fr/cri/docker/mirror/python:3.12-slim as base

ENV DEBIAN_FRONTEND=noninteractive \
    LC_ALL=C.UTF-8 \
    LANG=C.UTF-8 \
    PYTHONDONTWRITEBYTECODE=1 \
    POETRY_VIRTUALENVS_CREATE=false \
    VIRTUAL_ENV="/opt/venv" \
    PATH="/opt/venv/bin:$PATH"


RUN apt-get update && \
    apt-get install -y --no-install-recommends \
        awscli make \
        curl ca-certificates gnupg apt-transport-https \
        libpq-dev postgresql-client \
        libssl-dev libldap2-dev libsasl2-dev

FROM base as builder

RUN apt-get update && \
    apt-get install -y --no-install-recommends gcc python3-dev bison git

RUN --mount=type=bind,target=./pyproject.toml,src=./pyproject.toml \
    --mount=type=bind,target=./poetry.lock,src=./poetry.lock \
    --mount=type=cache,target=/root/.cache/pip \
    python -m venv /opt/venv && \
    pip3 install --upgrade pip && \
    pip3 install poetry==1.8.5 && \
    poetry install --only main

FROM registry.cri.epita.fr/cri/docker/mirror/node:20 as node-builder

RUN mkdir /app

WORKDIR /app

RUN --mount=type=bind,target=./package.json,src=./package.json \
    --mount=type=bind,target=./yarn.lock,src=./yarn.lock \
    yarn install --pure-lockfile --non-interactive --prod

FROM base

ARG WORKDIR=/app/fleet_manager
ARG DJANGO_UID=1000
ARG DJANGO_GID=1000

COPY --from=builder /opt/venv/ /opt/venv/

RUN mkdir /app && \
    useradd -d /app -r -u ${DJANGO_UID} app && \
    mkdir -p "$WORKDIR"

COPY --from=node-builder /app/node_modules /app/node_modules

RUN chown app:${DJANGO_GID} -R /app

USER app:${DJANGO_GID}
WORKDIR "$WORKDIR"

COPY ./ ./

ENTRYPOINT ["/app/fleet_manager/docker/entrypoint.sh"]
