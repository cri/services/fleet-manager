#!/usr/bin/env bash

set -eo pipefail

getsecret() {
  FILE_PATH="$(eval "echo -n \"\${${1}_FILE}\"")"
  if [ ! -z "${FILE_PATH}" ]; then
    cat "${FILE_PATH}"
  else
    eval "echo -n \"\${${1}}\""
  fi
}

export DB_USER="$(getsecret "DB_USER")"
export DB_PASSWORD="$(getsecret "DB_PASSWORD")"

envsubst < /etc/powerdns/pdns.conf.tmpl > /etc/powerdns/pdns.conf

if [ "$#" -eq 0 ]; then
  set -- pdns_server
fi

exec "$@"
