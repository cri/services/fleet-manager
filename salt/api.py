from django.urls import include, path
from rest_framework.routers import DefaultRouter

from .views import MinionViewSet

router = DefaultRouter()
router.root_view_name = "salt-api-root"
router.register(r"minions", MinionViewSet)

urlpatterns = [
    path("", include(router.urls)),
]
