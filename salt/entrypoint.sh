#!/usr/bin/env bash

set -eo pipefail

getsecret() {
  FILE_PATH="$(eval "echo -n \"\${${1}_FILE}\"")"
  if [ ! -z "${FILE_PATH}" ]; then
    cat "${FILE_PATH}"
  else
    eval "echo -n \"\${${1}}\""
  fi
}

export DB_USER="$(getsecret "DB_USER")"
export DB_PASSWORD="$(getsecret "DB_PASSWORD")"
export SALT_API_SHARED_SECRET="$(getsecret "SALT_API_SHARED_SECRET")"

envsubst < /etc/salt/master.tmpl > /etc/salt/master

mkdir -p /etc/salt/pki/master
echo "$(getsecret "SALT_PKI_MASTER_PRIVATE_KEY")" > /etc/salt/pki/master/master.pem
echo "$(getsecret "SALT_PKI_MASTER_PUBLIC_KEY")" > /etc/salt/pki/master/master.pub

if [ -n "$DEV" ] && [ "$#" -eq 0 ]; then
  salt-api &
fi

if [ "$#" -eq 0 ]; then
  set -- salt-master
fi

exec "$@"
