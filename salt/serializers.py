from rest_framework import serializers

from .models import Minion


class MinionSerializer(serializers.HyperlinkedModelSerializer):
    last_job = serializers.DateTimeField(source="last_job.alter_time", default=None)

    class Meta:
        model = Minion
        fields = "__all__"
        extra_kwargs = {
            "url": {"lookup_field": "minion_id"},
        }
