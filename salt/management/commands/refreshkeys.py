from django.core.management.base import BaseCommand

from salt.models import Key


class Command(BaseCommand):
    help = "Refresh all keys"

    def handle(self, *args, **options):
        Key._refresh_all()
