from django_filters.rest_framework import (
    DjangoFilterBackend as UpstreamDjangoFilterBackend,
)


class DjangoFilterBackend(UpstreamDjangoFilterBackend):
    def get_filterset_class(self, view, queryset=None):
        if hasattr(view, "get_filterset_class"):
            return view.get_filterset_class(queryset)
        return super().get_filterset_class(view, queryset)
