from django.contrib.auth.models import Group
from django.db import transaction
from social_auth_backend_epita.backend import EpitaOpenIdConnect


# pylint: disable=keyword-arg-before-vararg
def save_all_claims_as_extra_data(response, storage, social=None, *_args, **_kwargs):
    """Update user extra-data using data from provider."""
    if not social:
        return {}

    social.extra_data = response
    storage.user.changed(social)

    return {}


# pylint: disable=keyword-arg-before-vararg
def update_groups(backend, response, social, user=None, *_args, **_kwargs):
    """Update user groups using data from provider."""
    if not social:
        return {}
    if backend.name != EpitaOpenIdConnect.name:
        return {}
    if not user:
        return {}

    try:
        user_groups = []
        for role in response["roles"]:
            user_group, _ = Group.objects.get_or_create(name=role)
            user_groups.append(user_group)
        for group in response["groups"]:
            user_group, _ = Group.objects.get_or_create(name=group["slug"])
            user_groups.append(user_group)
        with transaction.atomic():
            user.groups.clear()
            user.groups.add(*set(user_groups))
    except Exception as e:
        raise ValueError from e

    return {}


# pylint: disable=keyword-arg-before-vararg
def set_permissions(backend, response, social, user=None, *_args, **_kwargs):
    """Update user groups using data from provider."""
    if not social:
        return {}
    if backend.name != EpitaOpenIdConnect.name:
        return {}
    if not user:
        return {}

    user.is_superuser = "superuser" in response.get("roles", [])
    user.is_staff = "staff" in response.get("roles", [])
    user.save()

    return {}
